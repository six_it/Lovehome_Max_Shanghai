
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Lovehome_Max_Shanghai.settings.dev')

from celery import Celery


celery_app = Celery('Lovehome_Max_Shanghai')


celery_app.config_from_object('celery_tasks.config')


celery_app.autodiscover_tasks(['celery_tasks.sms',])
