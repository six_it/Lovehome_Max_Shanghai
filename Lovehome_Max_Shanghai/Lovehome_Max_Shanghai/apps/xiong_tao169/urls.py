from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r'^api/v1.0/user/name$', views.ChangeUsernameView.as_view()),
    re_path(r'^api/v1.0/areas$', views.AreasListView.as_view()),
    re_path(r'^api/v1.0/user/houses$', views.MyListView.as_view()),
    # re_path(r'^api/v1.0/houses$', views.HoushListView.as_view()),
    re_path(r'^api/v1.0/user$', views.UserInfoView.as_view()),
    re_path(r'^api/v1.0/user/avatar$', views.AvatarView.as_view()),

]