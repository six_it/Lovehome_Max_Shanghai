from rest_framework import serializers

from six_it.models import User


class Userserializers(serializers.ModelSerializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(write_only=True)
    class Meta:
        model = User

        fields = ['password','mobile','username']

    def create(self, validated_data):

        return User.objects.create_user(username=validated_data.get('mobile'),
                                        password=validated_data.get('password'),
                                        mobile=validated_data.get('mobile'))