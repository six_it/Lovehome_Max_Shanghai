from django.urls import re_path
from . import views
urlpatterns = [
    re_path(r'^api/v1.0/users$',views.UsersCreateView.as_view()),
]