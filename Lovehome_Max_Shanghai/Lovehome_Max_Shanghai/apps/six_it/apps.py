from django.apps import AppConfig


class SixItConfig(AppConfig):
    name = 'six_it'
