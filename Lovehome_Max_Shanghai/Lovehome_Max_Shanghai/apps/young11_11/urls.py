from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^api/v1.0/orders$', views.OrderCommitView.as_view()),
    re_path(r'^api/v1.0/houses$', views.ReleaseHousesView.as_view()),
    re_path(r'^api/v1.0/houses/(?P<house_id>\d+)/images$', views.ImageHousesView.as_view()),

]
