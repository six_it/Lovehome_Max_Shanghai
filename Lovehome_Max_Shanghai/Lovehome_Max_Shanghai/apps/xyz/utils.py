from django.contrib.auth.backends import ModelBackend
from django.db.models import Q

from six_it.models import User


class Who(ModelBackend):
    """重写authenticate"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            user = User.objects.get(Q(username=username) | Q(mobile=username))
        except:
            user = None
        # 进行check_password判断密码是否正确
        if user and user.check_password(password):
            return user
