from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^api/v1.0/orders/(?P<order_id>\d+)/status$', views.AcceptanceRejectionView.as_view()),
    re_path(r'^api/v1.0/orders/(?P<order_id>\d+)/comment$', views.CommentOrdersView.as_view()),
    re_path(r'^api/v1.0/houses/index/$', views.HouseRecommendationView.as_view()),

]