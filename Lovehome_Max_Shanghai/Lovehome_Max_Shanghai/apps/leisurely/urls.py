from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r"^api/v1.0/imagecode$", views.ImageCodeView.as_view()),
    re_path(r"^api/v1.0/sms$", views.MobilesmsView.as_view())

]