from django.apps import AppConfig


class LeisurelyConfig(AppConfig):
    name = 'leisurely'
