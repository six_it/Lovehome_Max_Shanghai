from django.urls import re_path
from . import views

urlpatterns = [
    # re_path(r'^api/v1.0/areas$/', views.AreasView.as_view()),
    # re_path(r'^api/v1.0/user/avatar$/', views.AvatarView.as_view()),
    re_path(r'^api/v1.0/user/auth$', views.AuthView.as_view()),
    re_path(r'^api/v1.0/houses/(?P<house_id>\d+)$', views.HouseDetailView.as_view()),
]
